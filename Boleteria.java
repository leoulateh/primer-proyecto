
import javax.swing.JOptionPane;
public class Boleteria {

private Sala sala=new Sala();
private Pelicula pelicula=new Pelicula();
private Caja caja=new Caja();
 
public Boleteria(){
 
 crearMenu();
 
 }//fin metodo boleteria
 
public void crearMenu (){
 
 char opcion;
 int condicion=1;
 do {
  

  
  opcion=(JOptionPane.showInputDialog("***** Boleteria *****\n"+
  "a. Vender Boletos \n"+
  "b. Devolver entradas \n"+
  "c. Ver disponibilidad de espacio \n"+
  "d. Ver informacion de la pelicula\n"+
  "e. Cerrar boleteria\n"+
  "f. Ver total recaudado\n"+
  "g. Salir")).charAt(0);
  
  switch (opcion)
  {
   
   case 'a':
   {
    if (condicion==1){
    
    int venta;
    int total;   
    
    venta=Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de entradas por comprar"));
    
    sala.setAsientosReservados(sala.getAsientosReservados()+venta);
    
    total=venta*1000;
    
    JOptionPane.showMessageDialog(null,"El total a pagar es: "+total);
   }//fin is
   else {
    
    JOptionPane.showMessageDialog(null,"Esta opcion no esta disponible.");
    }//fin else
    }break;
   
   case 'b':
   {
    if (condicion==1){
    int devolucion;
    int total;
    
    devolucion=Integer.parseInt(JOptionPane.showInputDialog("Digite la cantidad de entradas por devolver"));
    
    sala.setAsientosReservados(sala.getAsientosReservados()-devolucion);
    
    total=devolucion*1000;
    
    JOptionPane.showMessageDialog(null,"El total devuelto es: "+total);
   }//fin if
   else {
    JOptionPane.showMessageDialog(null,"Esta opcion no esta disponible.");
    }//fin else
    } break;
   
   case 'c':
   {
    int disponibilidad=sala.getCapacidad()-sala.getAsientosReservados();
    JOptionPane.showMessageDialog(null,"Los espacios disponibles para esta sala: "+disponibilidad);
    } break;
   
   case 'd':
   {
    JOptionPane.showMessageDialog(null, "Informacion de la pelicula: \n"+pelicula.toString());
    } break;
   
   case 'e':
   {
    condicion=0;
    JOptionPane.showMessageDialog(null, "Boleteria Cerrada.");
    } break;
   
   case 'f':
   {
    caja.setRecaudado(sala.getAsientosReservados()*1000);
    JOptionPane.showMessageDialog(null,"El total recaudado para esta funci�n es de"+caja.getRecaudado());
    } break;
   
   case 'g':
   {
    JOptionPane.showMessageDialog(null,"Gracias por usar el sistema de Boleteria CMRR");
    opcion='g';
    } break;
    
   
   default: JOptionPane.showMessageDialog(null,"Escoja una opcion valida del menu");
   }//fin switch
  
  } while (opcion!='g');
 
 }//fin metodo crear menu
public static void main (String arg[]){ 
 
 Boleteria boleteria=new Boleteria();
 }//fin metodo main
}//fin clase
