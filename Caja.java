public class Caja {

private double recaudado;

public Caja (){
	
	recaudado=0;
	
	}//fin metodo constructor sin parametros
	
public Caja (double recaudado){
	
	this.recaudado=recaudado;
	
	}//fin metodo constructor sin parametros
	
public void setRecaudado(double recaudado){
	
	this.recaudado=recaudado;
	
	}//fin metodo setRecaudado
	
public double getRecaudado(){
	
	return recaudado;
	
	}//fin metodo getRecaudado
}//fin clase
